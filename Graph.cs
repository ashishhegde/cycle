﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections;

namespace Cycle
{
    public partial class Graph : Form
    {
        public Graph()
        {
            InitializeComponent();
        }

        //For displaying HR data
        delegate void SetTextCallback(string text);
        //delegate void axisChangeZedGraphCallBack(ZedGraphControl zg);
        public Thread garthererThread;

        string textData = File.ReadAllText("ASDBExampleCycleComputerData.hrm");
        string[] arrayData;
        int index;
      

        public void Thread1()
        {
            arrayData = textData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            index = Array.IndexOf(arrayData, "[HRData]");
            for (int i = index + 1; i < arrayData.Length - 1; i++)
            {
                string HRData = arrayData[i];
                string[] arrHrdata = Regex.Split(HRData, @"\W+");
                this.SetText1(arrHrdata[0]);
                Thread.Sleep(1500);
            }
        }

        public void Thread2()
        {
            arrayData = textData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            index = Array.IndexOf(arrayData, "[HRData]");
            for (int i = index + 1; i < arrayData.Length - 1; i++)
            {
                string HRData = arrayData[i];
                string[] arrHrdata = Regex.Split(HRData, @"\W+");
                this.SetText2(arrHrdata[1]);
                Thread.Sleep(1500);
            }
        }

        public void Thread3()
        {
            arrayData = textData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            index = Array.IndexOf(arrayData, "[HRData]");
            for (int i = index + 1; i < arrayData.Length - 1; i++)
            {
                string HRData = arrayData[i];
                string[] arrHrdata = Regex.Split(HRData, @"\W+");
                this.SetText3(arrHrdata[2]);
                Thread.Sleep(1500);
            }
        }

        public void Thread4()
        {
            arrayData = textData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            index = Array.IndexOf(arrayData, "[HRData]");
            for (int i = index + 1; i < arrayData.Length - 1; i++)
            {
                string HRData = arrayData[i];
                string[] arrHrdata = Regex.Split(HRData, @"\W+");
                this.SetText4(arrHrdata[3]);
                Thread.Sleep(1500);
            }
        }

        public void Thread5()
        {
            arrayData = textData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            index = Array.IndexOf(arrayData, "[HRData]");
            for (int i = index + 1; i < arrayData.Length - 1; i++)
            {
                string HRData = arrayData[i];
                string[] arrHrdata = Regex.Split(HRData, @"\W+");
                this.SetText5(arrHrdata[4]);
                Thread.Sleep(1500);
            }
        }

        public void Thread6()
        {
            arrayData = textData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            index = Array.IndexOf(arrayData, "[HRData]");
            for (int i = index + 1; i < arrayData.Length - 1; i++)
            {
                string HRData = arrayData[i];
                string[] arrHrdata = Regex.Split(HRData, @"\W+");
                this.SetText6(arrHrdata[5]);
                Thread.Sleep(1500);
            }
        }

        private void SetText1(string text)
        {
            if (label1.InvokeRequired)
            {
                SetTextCallback st = new SetTextCallback(SetText1);
                this.Invoke(st, new object[] { text });
            }
            else
            {
                label1.Text = text;
            }
        }

        private void SetText2(string text)
        {
            if (label2.InvokeRequired)
            {
                SetTextCallback st = new SetTextCallback(SetText2);
                this.Invoke(st, new object[] { text });
            }
            else
            {
                label2.Text = text;
            }
        }

        private void SetText3(string text)
        {
            if (label3.InvokeRequired)
            {
                SetTextCallback st = new SetTextCallback(SetText3);
                this.Invoke(st, new object[] { text });
            }
            else
            {
                label3.Text = text;
            }
        }

        private void SetText4(string text)
        {
            if (label4.InvokeRequired)
            {
                SetTextCallback st = new SetTextCallback(SetText4);
                this.Invoke(st, new object[] { text });
            }
            else
            {
                label4.Text = text;
            }
        }

        private void SetText5(string text)
        {
            if (label5.InvokeRequired)
            {
                SetTextCallback st = new SetTextCallback(SetText5);
                this.Invoke(st, new object[] { text });
            }
            else
            {
                label5.Text = text;
            }
        }

        private void SetText6(string text)
        {
            if (label6.InvokeRequired)
            {
                SetTextCallback st = new SetTextCallback(SetText6);
                this.Invoke(st, new object[] { text });
            }
            else
            {
                label6.Text = text;
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void Graph_Load_1(object sender, EventArgs e)
        {
            Thread th1 = new Thread(new ThreadStart(Thread1));
            th1.Start();

            Thread th2 = new Thread(new ThreadStart(Thread2));
            th2.Start();

            Thread th3 = new Thread(new ThreadStart(Thread3));
            th3.Start();

            Thread th4 = new Thread(new ThreadStart(Thread4));
            th4.Start();

            Thread th5 = new Thread(new ThreadStart(Thread5));
            th5.Start();

            Thread th6 = new Thread(new ThreadStart(Thread6));
            th6.Start();
        }

    }
}
